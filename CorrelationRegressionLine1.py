# y= -3/4*x-2 +e        (1);
# x=-3/4*y -7/4 +e      (2); 

#The linear regression coefficient beta is Sxy/Sxx ( in y=beta*x+e format),
# So, applying the regular beta formular to our problem,
#  we get Sxy/Sxx= -3/4 from (1) ......(3); 
#  and Syx/Syy=-3/4 from (2) ........(4); 
#  Recall that the Peason corelation is: r=Sxy/(sqrt(Sxx)*sqrt(Syy)) 
#  Here Sxy=Syx. So, if we multiply (3) and (4) and sqrt the result, 
#  we get: sqrt(Sxy*Syx/(Sxx*Syy))=r=+ or - 3/4. 
#  Since y and x are negtive corellated based on the negative beta, 
#  we get the r = -0.75.
