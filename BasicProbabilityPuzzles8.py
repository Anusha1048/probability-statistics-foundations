from fractions import Fraction

#𝑃(A union B)=𝑃(A)+𝑃(B)−𝑃(A intersection B)

# don't read morning news = 1/2
# read morning news = 1-1/2 =1/2

# don't read evening news = 2/5
# read evening news = 1-2/5 = 3/5

# read both news = 1/5
#Find the probability that a resident reads a morning or evening newspaper.

#p(morning or evening) = p(morning)+p(evening)-p(morning+evening)

probability =1/2+3/5-1/5
print(Fraction(probability).limit_denominator(100))

