from fractions import Fraction

#The number of ways to arrange n distinct objects along a fixed circle is 
#p(n)=(n-1)!
# One person will sit, leaving nine positions. Each position has two 
# seats, one on the left side and one on the right side. 

# Result =  2/9 = 0.222222222 
print(Fraction(2/9).limit_denominator(100))
