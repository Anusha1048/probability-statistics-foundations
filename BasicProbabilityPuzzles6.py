from fractions import Fraction

#The probability of drawing a black ball from bag X and then a black ball from bag Y:
# 4 / 5+4   * (6+1) / (7+6+1) = 28/126
#The probability of drawing a white ball from bag X and then a black ball from bag Y:
# 5/5+4 * 6/7+6+1 =30/126

# 28/126 + 30/126 = 58/126 = 23/63
print(Fraction(58/126).limit_denominator(100))
