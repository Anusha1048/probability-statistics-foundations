from fractions import Fraction
# a = event that husband is selected
# b = event that the wife is selected
pa = 1/3
pb = 1/5
pa_bar = 1-pa
pb_bar = 1-pb

# probability = p(A and not B ) or (B and not A)
probability = pa * pb_bar + pb * pa_bar
print(Fraction(probability).limit_denominator(100))