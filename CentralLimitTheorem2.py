import math

def central_limit_theorem(x, mean, sd):
  return 0.5 * (1 + math.erf((x - mean) / (sd * (2 ** 0.5))))


nb_tickets = 250
nb_students = 100
mean = 2.4
std = 2.0

mean_sample = mean * nb_students
sd_sample = std * math.sqrt(nb_students)

print(round(central_limit_theorem(nb_tickets, mean_sample, sd_sample), 4))