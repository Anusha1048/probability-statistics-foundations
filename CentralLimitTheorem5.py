import math
def central_limit_theorem(x, mean, sigma):
    return 0.5 + math.erf((x-mean)/(sigma*2**0.5))*0.5

mean = 50000
sigma = 10000

weeks=11
start = 74000
weekly = 47000
end = start + weekly*weeks
lower_bound = end - 20000

sum_mean = weeks*mean
sum_sigma = weeks**0.5*sigma

print('{:0.4f}'.format(1-central_limit_theorem(lower_bound, sum_mean, sum_sigma)))