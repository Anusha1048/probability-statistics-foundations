# Let X and Y be two independent "normal" random deviates with mean and standard deviation 10 and 3,
# and 20 and 4, respectively. Let sigma be the value of the standard deviation of the distribution
# obtained by adding the two distributions ( X - Y ).

import math

mean1 = 10
mean2 = 20
sd1 = 3
sd2 = 4
# variance sum law: (SD of x-y)^2=(SD of x)^2 +(SD of y)^2
print('%.1f' % math.sqrt(sd1**2 + sd2**2))