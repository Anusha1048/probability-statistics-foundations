import math
#  Poisson distribution f(x=k,lambda)= (lambda ^ k) * (e ^ -2.5) / k!
# P(x; μ) = (e-μ) (μx) / x!

k=5
factorial_k = 120
_lambda = 2.5
result = (pow(_lambda,k)* pow(2.71828,-2.5)) / factorial_k
print('%.3f' % result)