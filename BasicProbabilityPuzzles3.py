from fractions import Fraction
x_prob_red = 4/7
x_prob_black = 3/7

y_prob_red = 5/9
y_prob_black = 4/9

z_prob_red = 1/2
z_prob_black = 1/2

# We have multiplied the possibilities
#What is the probability that the 3 balls drawn consist of 2 red balls and 1 black ball? 
first_combination   = x_prob_red * y_prob_red * z_prob_black
second_combination  = x_prob_black * y_prob_red * z_prob_red
third_combination   = x_prob_red * y_prob_black * z_prob_red

result=first_combination + second_combination + third_combination

# Result = 0.40476190476190477 = 17/42
print(Fraction(result).limit_denominator(100))