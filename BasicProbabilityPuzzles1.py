import math
from fractions import Fraction 
result = 0
# First dice
for i in range(1, 7):
    # Second dice
    for j in range(1, 7):
        #  find the probability of that their sum will be at most 9
        if (i + j) <= 9:
            result += 1/36
# Final probability found
print(Fraction(result).limit_denominator(10))