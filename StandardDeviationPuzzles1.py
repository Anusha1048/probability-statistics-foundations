# Standard DEviation
# sigma = sqrt ( E(x-mean)**2   / n )

import math

def mean(numbers):
    return sum(numbers)/len(numbers)

def standard_deviation(numbers ,n):
    sum = 0
    for i in range(n):
        sum = sum + (numbers[i]-mean(numbers)) ** 2
    return math.sqrt(sum/n)

n = int(input())
numbers = list(map(int, input().split()))

# Get standard deviation
print(round(standard_deviation(numbers, n), 2))