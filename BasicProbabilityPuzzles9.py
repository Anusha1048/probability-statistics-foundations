from fractions import Fraction
#required probability =1−P(AˉnBˉn Cˉ) 
#  P(A)=1/12, P(B)=1/15 and P(C)=1/10
pa=1/12
pb=1/15
pc=1/10
probability = 1-((1-pa)*(1-pb)*(1-pc))
print(Fraction(probability).limit_denominator(100))