import math
from fractions import Fraction 
result = 0
# First dice
for i in range(1, 7):
    # Second dice
    for j in range(1, 7):
        #  ind the probability that the values rolled by each die will be different and their sum is 6. 
        if(i!=j):
            if (i + j) == 6:
                result += 1/36
# Final probability found
print(Fraction(result).limit_denominator(10))