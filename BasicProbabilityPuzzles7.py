from fractions import Fraction
A_defects =  500 * 0.005 =  2.5
B_defects = 1000 * 0.008 =  8
C_defects = 2000 * 0.010 = 20
#a = selecting defective
#b = from plant A
p(b/a)  = p(a/b) * p(b) /  p(a)

Sum_defects= A_defects + B_defects +C_defects
probability = A_defects / Sum_defects 
print(Fraction(probability).limit_denominator(100))
