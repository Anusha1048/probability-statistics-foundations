# formula for Central Limit Theoremimport math

def central_limit_theorem(x, mean, sd):
  return 0.5 * (1 + math.erf((x - mean) / (sd * (2 ** 0.5))))

max = 9800
n = 49
mean = 205
sd = 15
mean_sample = mean * n
sd_sample = math.sqrt(n) * sd
print(round(central_limit_theorem(max, mean_sample, sd_sample), 4))