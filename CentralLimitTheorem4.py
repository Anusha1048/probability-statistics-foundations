import math

n=100
mean = 500
sigma=80
sample_mean=mean
sample_sigma=sigma/math.sqrt(n)
z = 1.96

A = sample_mean - z*sample_sigma
B = sample_mean + z*sample_sigma
print(A,B, sep='\n', end='')