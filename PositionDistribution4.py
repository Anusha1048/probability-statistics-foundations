# import math
# def fact(n):
#     return 1 if n == 0 else n*fact(n-1)
# def poi(k,lam):
#     return ((lam**k)*(math.exp(-lam)))/(fact(k))

# #1
# lam = 1.2
# result = poi(2,1.2)
# print('%.3f' % result)

# #2
# result = poi(0,1.2) + poi(1,1.2) + poi(2,1.2)
# print('%.3f' % result)
# #or
# # k = 2; lam = 1.2
# # result = sum([poi(x,lam) for x in range(0,k+1)])
# # print('%.3f' % result)


# #3
# lam = 12; k = 5 
# result = poi(k,lam)
# print('%.3f' % result)


# #4
# k = 40; lam = 1.2
# result = sum([poi(x,lam) for x in range(0,k+1)])
# print('%.3f' % result)

import math,scipy.stats

print('%.3f'%scipy.stats.poisson.pmf(2, 1.2))
print('%.3f'%scipy.stats.poisson.cdf(2, 1.2))
print('%.3f'%scipy.stats.poisson.pmf(5, 1.2*10))
print('%.3f'%(1-scipy.stats.poisson.cdf(2, 1.2*40)))