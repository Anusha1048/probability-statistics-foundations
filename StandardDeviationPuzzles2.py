# z = (x – μ) / σ 
# For example, let’s say you have a test score of 190. The test has a mean (μ) of 150 and a standard deviation (σ) of 25. Assuming a normal distribution, your z score would be: 
mean = 0.675
sd = 0.065
xi = 0.9025

z = (xi - mean)/sd

print('%.2f' % (z))