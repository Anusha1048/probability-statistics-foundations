# slope = Sdps/Sds. 

#R^2 = 0.4 = (Sdsp)/(Sds * Sdp).

# Sds = (standard deviation of S)^2 = (4)^2 
# Sdp = (standard deviation of P)^2 = (8)^2.

# Sdsp = (deviation between S and P)

# Solving for Sdsp from the R^2 equation  Sdsp = (Sds * Sdp) * R^2 = (4^2 * 8^2 * 0.4)

# Slope = Sdps/Sds = (4^2 * 8^2 * 0.4)/(4^2)  = 1.26. Which is the answer. 
