import math

def central_limit_theorem(x, mean, sd):
  return 0.5 * (1 + math.erf((x - mean) / (sd * (2 ** 0.5))))

mean = 500
sd = 80
sample_size = 100

mean_sample = mean * sample_size
sd_sample = math.sqrt(sample_size) * sd

p1 = central_limit_theorem(510*sample_size, mean_sample, sd_sample)
p2 = central_limit_theorem(490*sample_size, mean_sample, sd_sample)


print(round(p1 - p2, 4))