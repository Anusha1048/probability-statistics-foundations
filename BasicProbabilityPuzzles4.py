from fractions import Fraction

# Combinations
# Bag1	Bag2	Bag2
# black	black	red
# black	red	    black
# red	black	black

# We have multiplied the possibilities
first_combination   = (5/9) * (7/10) * (3/9)
second_combination  = (5/9) * (3/10) * (7/9)
third_combination   = (4/9) * (7/10) * (6/9)

result=first_combination + second_combination + third_combination
# Result = 0.40476190476190477 = 17/42
print(Fraction(result).limit_denominator(100))
